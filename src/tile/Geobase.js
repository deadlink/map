L.TileLayer.Geobase = L.TileLayer.extend({
    getTileUrl: function (tilePoint) {
        var offset = Math.pow(2, tilePoint.z - 1);
        return L.Util.template(this._url, L.extend({
            s: this._getSubdomain(tilePoint),
            z: Math.pow(2, tilePoint.z - 2),
            x: tilePoint.x - offset,
            y: tilePoint.y - offset,
            n: 4
        }, this.options));
    }
});
L.TileLayer.geobase = function(options) {
    var url = 'http://{server_ip}/GeoStream/tile.aspx?t={x},{y},256,{n},{z}';
    var defaultOptions = {
        minZoom: 2,
        maxZoom: 18,
        noWrap: true,
        attribution: '&copy; geobase'
    };
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.TileLayer.Geobase(url, defaultOptions);
};
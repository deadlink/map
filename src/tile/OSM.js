L.TileLayer.osm = function(options) {
    var url = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    var defaultOptions = {
        subdomains: 'abc',
        noWrap: true,
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    };
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.FastTileLayer(url, defaultOptions);
};
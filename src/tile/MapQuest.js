L.TileLayer.mapQuest = function(options) {
    var url = 'http://otile{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png';
    var defaultOptions = {
        subdomains: '1234',
        noWrap: true,
        attribution: 'Data CC-By-SA by <a href="http://openstreetmap.org/" target="_blank">OpenStreetMap</a>, ' +
            'Tiles Courtesy of <a href="http://open.mapquest.com" target="_blank">MapQuest</a>'
    }
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.TileLayer(url, defaultOptions);
};
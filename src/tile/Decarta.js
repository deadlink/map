L.TileLayer.Decarta = L.TileLayer.extend({
    getTileUrl: function (tilePoint) {
        var offset = Math.pow(2, tilePoint.z - 1);
        return L.Util.template(this._url, L.extend({
            s: this._getSubdomain(tilePoint),
            z: tilePoint.z,
            x: tilePoint.x - offset,
            y: -tilePoint.y + offset - 1
        }, this.options));
    }
});
L.TileLayer.decarta = function(options) {
    var url = "http://vip5dzsv-0{s}.decartahws.com/openls/image-cache/TILE/{x}/{y}/{z}?CLIENTNAME=map-sample-app&CONFIG=global-decarta&P=EPSG:3857";
    var defaultOptions = {
        subdomains: '123',
        minZoom: 1,
        maxZoom: 18,
        noWrap: true,
        attribution: '&copy; decarta'
    };
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.TileLayer.Decarta(url, defaultOptions);
};
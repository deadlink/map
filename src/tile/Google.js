L.TileLayer.google = function(options) {
    var url = 'http://mt{s}.googleapis.com/vt/?hl=ru&src=api&x={x}&y={y}&z={z}&s=';
    var defaultOptions = {
        subdomains: '123',
        noWrap: true,
        attribution: '&copy; <a href="http://google.com">Google Inc</a>'
    }
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.FastTileLayer(url, defaultOptions);
};
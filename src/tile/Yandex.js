L.TileLayer.yandex = function(options) {
    var url = 'http://{type}0{s}.maps.yandex.ru/tiles?l=map&v={version}&x={x}&y={y}&z={z}';
    var defaultOptions = {
        crs: L.CRS.EPSG3395,
        version: '2.43.0',
        type: 'vec',
        minZoom: 0,
        maxZoom: 17,
        subdomains: '123',
        noWrap: true,
        attribution: '&copy; Яндекс, &copy; Роскартография, &copy; ЗАО «Резидент», &copy; ЗАО «ТГА», 2006'
    }
    for (var option in options) {
        defaultOptions[option] = options[option];
    }
    return new L.TileLayer(url, defaultOptions);
};

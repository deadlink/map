L.TileLayer.nokiaHere = function(options) {
    var url = 'http://{s}.maps.nlp.nokia.com/maptile/2.1/maptile/newest/normal.day/{z}/{x}/{y}/256/png8?lg={lang}&app_id={appid}&token={token}';
    var defaultOptions = {
        subdomains: '1234',
        noWrap: true,
        attribution: '&copy; <a href="http://here.com">Nokia</a>'
    }
    for (var option in options) {
        defaultOptions[option] = options[option];
    }

    return new L.TileLayer(url, defaultOptions);
};


L.Store = L.Class.extend({

    statics: {
        store: {},
        injection: {
            getStore: function(name) {
                if (!(name in L.Store.store)) {
                    L.Store.store[name]
                }
                return L.Store.store[name] = new L.Store(this);
            }
        }
    },

    initialize: function(map) {
        this._map = map;
        this.clear();
    },

    clear: function() {
        this._index = new RTree();
    },

    add: function(bounds, data) {
        return this._index.insert(L.Util.boundsToRtreeSearchObject(bounds), data);
    },

    remove: function(bounds) {
        return this._index.remove(L.Util.boundsToRtreeSearchObject(bounds));
    },

    search: function(bounds, return_node, return_array) {
        if (return_node) {
            return this._index.search(L.Util.boundsToRtreeSearchObject(bounds), return_node, return_array);
        }
        return this._index.search(L.Util.boundsToRtreeSearchObject(bounds));
    }
});

L.Map.include(L.Store.injection);
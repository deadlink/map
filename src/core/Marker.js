L.Marker.include({
    // TODO нужно ли? Селекшены лучше обрабатывать на уровне DOM и иконку маркера брать в виде спрайта
    _selected: false,
    isSelected: function() {
        return this._selected;
    },
    select: function() {
        this._selected = true;
        if (this.options.selectedIcon) {
            this.setIcon(this.options.selectedIcon);
        }
    },
    unselect: function() {
        this._selected = false;
        if (this.options.selectedIcon) {
            this.setIcon(this.options.icon);
        }
    }
});

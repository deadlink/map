L.ContextMenu = L.Class.extend({

    includes: [L.Mixin.Events],

    statics: {
        activeMenu: null,
        injection: {
            bindMenu: function(/*L.ContextMenu*/contextMenu) {
                if (contextMenu instanceof L.ContextMenu) {
                    var scope = this;
                    this.on("contextmenu", function(ev) {
                        contextMenu.show(ev, scope);
                        L.DomEvent.stopPropagation(ev);
                    });
                } else {
                    throw 'L.ContextMenu instance required';
                }
                return this;
            }
        }
    },

    options: {
        className: 'leaflet-contextmenu',
        items: {}
    },

    initialize: function(options) {
        if (!!!(this.options.items)) {
            throw 'Menu has required: items';
        }
        L.setOptions(this, options);
    },

    onAdd: function(map) {
        this._map = map;
        this._build();
    },

    addTo: function (map) {
        map.addLayer(this);
        return this;
    },

    _build: function() {
        var map = this._map,
            options = this.options;
        this._container = L.DomUtil.create('ul', options.className, map._container);
        L.DomEvent.addListener(this._container, 'mousedown', function(ev){
            L.DomEvent.stopPropagation(ev);
        });
        for (var item in options.items) {
            this._addItem(item, options.items[item])
        }
    },

    _addItem: function(name, listener) {
        var options = this.options;
        var item = L.DomUtil.create('li', options.className+'-item', this._container),
            scope = this;
        item.innerHTML = name;
        L.DomEvent.addListener(item,'click', function(ev) {
            L.DomEvent.stopPropagation(ev);
            scope._hide();
            listener.apply(scope, [scope._target]);
        });
    },

    _hide: function() {
        var map = this._map;
        map.off('click movestart zoomstart', this._hide);
        map.off('mouseweel', L.DomEvent.stopPropagation);
        this._container.style.visibility = 'hidden';
    },

    show: function(ev, target) {
        var map = this._map;
        if (L.ContextMenu.activeMenu) {
            L.ContextMenu.activeMenu._hide();
        }
        L.ContextMenu.activeMenu = this;
        if (ev.containerPoint) {
            this._latlng = ev.latlng;
            this._point = ev.containerPoint;
        } else {
            this._latlng = ev.target._latlng;
            this._point = this._map.latLngToContainerPoint(this._latlng);
        }
        this._target = target;
        var width = this._container.clientWidth;
        var height = this._container.clientHeight;
        if (this._point.x + width + 10 >= this._map._container.clientWidth) {
            this._point.x -= width + 10;
        }
        if (this._point.y + height + 10 >= this._map._container.clientHeight) {
            this._point.y -= height + 10;
        }
        L.DomUtil.setPosition(this._container, this._point, true);
        map.on('click movestart zoomstart', this._hide, this);
        this._container.style.visibility = 'visible';
    },

    getPoint: function() {
        return this._point;
    },

    getLatLng: function() {
        return this._latlng;
    }
});

L.contextMenu = function(options) {
    return new L.ContextMenu(options);
};

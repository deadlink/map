L.MaxoptraConfig = L.Config.extend({

    initialize: function(params) {
        this.center = params.center;
        this.activeBaseLayer = params.defaultMap.toLowerCase();
        for (var provider in params.mapProviders) {
            if (provider in this._baseLayers) {
                this.availableLayers[provider] = params.mapProviders[provider];
            }
        }
    }

});
L.Map.mergeOptions({
    boxSelection: false
});

L.Map.BoxSelection = L.Handler.extend({

    initialize: function (map) {
        this._map = map;
        this._container = map._container;
        this._pane = map._panes.overlayPane;
    },

    addHooks: function () {
        L.DomEvent.on(this._container, 'mousedown', this._onMouseDown, this);
    },

    removeHooks: function () {
        L.DomEvent.off(this._container, 'mousedown', this._onMouseDown);
    },

    _onMouseDown: function (e) {
        if (!e.ctrlKey || ((e.which !== 1) && (e.button !== 1))) { return false; }

        L.DomUtil.disableTextSelection();

        this._startLayerPoint = this._map.mouseEventToLayerPoint(e);

        this._box = L.DomUtil.create('div', 'leaflet-box-selection', this._pane);
        L.DomUtil.setPosition(this._box, this._startLayerPoint);

        this._container.style.cursor = 'crosshair';

        L.DomEvent
            .on(document, 'mousemove', this._onMouseMove, this)
            .on(document, 'mouseup', this._onMouseUp, this)
            .on(document, 'keydown', this._onKeyDown, this)
            .preventDefault(e);

        this._map.fire('boxselectionstart');
    },

    _onMouseMove: function (e) {
        var map = this._map,
            startPoint = this._startLayerPoint,
            box = this._box,

            layerPoint = this._map.mouseEventToLayerPoint(e),
            offset = layerPoint.subtract(startPoint),

            newPos = new L.Point(
                Math.min(layerPoint.x, startPoint.x),
                Math.min(layerPoint.y, startPoint.y));

        L.DomUtil.setPosition(box, newPos);

        var width = (Math.max(0, Math.abs(offset.x) - 4)) + 'px';
        var height = (Math.max(0, Math.abs(offset.y) - 4)) + 'px';
        var oldWidth = box.style.width;
        var oldHeight = box.style.height;
        box.style.width  = width;
        box.style.height = height;

        if (Math.abs(parseInt(width) - parseInt(oldWidth)) > 0 || Math.abs(parseInt(height) - parseInt(oldHeight)) > 0) {
            map.fire('boxselectionresize', {
                bounds: new L.LatLngBounds(
                    map.layerPointToLatLng(this._startLayerPoint),
                    map.layerPointToLatLng(layerPoint))
            });
        }

    },

    _finish: function () {
        this._pane.removeChild(this._box);
        this._container.style.cursor = '';

        L.DomUtil.enableTextSelection();

        L.DomEvent
            .off(document, 'mousemove', this._onMouseMove)
            .off(document, 'mouseup', this._onMouseUp)
            .off(document, 'keydown', this._onKeyDown);
    },

    _onMouseUp: function (e) {

        this._finish();

        var map = this._map,
            layerPoint = map.mouseEventToLayerPoint(e);

        if (this._startLayerPoint.equals(layerPoint)) { return; }

        map.fire('boxselectionend', {
            bounds: new L.LatLngBounds(
                map.layerPointToLatLng(this._startLayerPoint),
                map.layerPointToLatLng(layerPoint))
        });
    },

    _onKeyDown: function (e) {
        if (e.keyCode === 27) {
            this._finish();
        }
    }
});

L.Map.addInitHook('addHandler', 'boxSelection', L.Map.BoxSelection);

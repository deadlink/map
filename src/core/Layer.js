L.TileLayer.include({
    getCRS: function() {
        if (this.options.crs) {
            return this.options.crs;
        }
        // 3857 by default
        return L.CRS.EPSG3857;
    }
});
L.LayerGroup.include({
    clear: function() {
        this.clearLayers();
    },
    show: function() {
        if (this._owner) {
            this.addTo(this._owner);
        }
    },
    hide: function() {
        this._owner = this._map;
        this._map.removeLayer(this);
    },
    setName: function(name) {
        this._name = name;
    },
    getName: function() {
        return this._name;
    }
});

L.FastTileLayer = L.TileLayer.extend({

    _load: function(url, tile) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function(e) {
            var _xhr = e.target;
            var dataView = new DataView(_xhr.response);
            var blob = new Blob([dataView], {type: "image/png"});
            tile.src = URL.createObjectURL(blob);
        };
        xhr.open("GET", url, true);
        xhr.responseType = "arraybuffer";
        xhr.timeout = 1000;
        xhr.send(null);
    },

    _loadTile: function (tile, tilePoint) {
        tile._layer  = this;
        tile.onload  = this._tileOnLoad;
        tile.onerror = this._tileOnError;
        this._adjustTilePoint(tilePoint);
        if (this.options.useAjax) {
            tile.src = 'empty.png';
            this._load(this.getTileUrl(tilePoint), tile);
        } else {
            tile.src = this.getTileUrl(tilePoint);
        }
    },

    _addTilesFromCenterOut: function (bounds) {
        var queue = [], j, i, point;
        for (j = bounds.min.y; j <= bounds.max.y; j++) {
            for (i = bounds.min.x; i <= bounds.max.x; i++) {
                point = new L.Point(i, j);
                if (this._tileShouldBeLoaded(point)) {
                    queue.push(point);
                }
            }
        }
        var tilesToLoad = queue.length;
        if (tilesToLoad === 0) { return; }
        var fragment = document.createDocumentFragment();
        if (!this._tilesToLoad) {
            this.fire('loading');
        }
        this._tilesToLoad += tilesToLoad;
        for (i = 0; i < tilesToLoad; i++) {
            this._addTile(queue[i], fragment);
        }
        this._tileContainer.appendChild(fragment);
    },

    _addTile: function (tilePoint, container) {
        var tilePos = this._getTilePos(tilePoint);
        var tile = this._getTile();
        L.DomUtil.setPosition(tile, tilePos, true);//?
        this._tiles[tilePoint.x + ':' + tilePoint.y] = tile;
        L.Util.requestAnimFrame(function() {
            this._loadTile(tile, tilePoint);
        }, this, container);
        if (tile.parentNode !== this._tileContainer) {
            container.appendChild(tile);
        }
    }

});


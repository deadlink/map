L.Map.addInitHook(function(){
    this.on('baselayerchange', function(ev){
        if (ev.layer.getCRS() != this.options.crs) {
            var center = this.getCenter();
            var zoom = this.getZoom();
            this.options.crs = ev.layer.getCRS();
            this._resetView(center, zoom, false, false);
        }
    });
    this.controls = {};
    this.controls.layers = L.control.layers();
    this.controls.layers.addTo(this);
    L.control.scale().addTo(this);
});

L.Map.include({
    router: null,
    addOverlay: function(layer, name) {
        this.controls.layers.addOverlay(layer, name);
        return this;
    },
    addBaseLayer: function(layer, name) {
        this.controls.layers.addBaseLayer(layer, name);
        return this;
    },
    setConfig: function(config /*L.Config*/) {
        this.config = config;
        var baseLayers = config.getBaseLayers();
        for (var layer in baseLayers) {
            this.addBaseLayer(baseLayers[layer], layer);
        }
        this.addLayer(baseLayers[config.getActiveBaseLayer()]);
        this.setView(config.getCenter(), config.getZoom());
        this.setRouter(new L.Router(config));
        return this;
    },
    getRouter: function() {
        return this.router;
    },
    setRouter: function(router) {
        this.router = router;
        return this;
    },

    _animatedFly: function() {
        var zoom = this.getZoom(), center = this.getCenter();
        // отзумимся до того, пока сможем видеть место, куда нужно перелететь
        if (!this.getBounds().contains(this._flyTo.latlng)) {
            this._resetView(center, zoom - 0.2);
        } else {
            // нужен переезд в новый центр?
            if (!this._flyTo.panEnd) {
                this._resetView(center, Math.ceil(zoom));
                this.on('moveend', this._animatedFlyPanEnd, this);
                this.panTo(this._flyTo.latlng);
                return;
            // Уже переехали =)
            } else {
                // призумимся до нужного уровня
                if (zoom < this._flyTo.zoom) {
                    this._resetView(this._flyTo.latlng, zoom + 0.2);
                    // завершим полет
                } else {
                    this._resetView(this._flyTo.latlng, this._flyTo.zoom);
                    this._flyTo.zoomEnd = true;
                }
            }
        }
        if (!this._flyTo.zoomEnd) {
            L.Util.requestAnimFrame(this._animatedFly, this, true, this._container);
        }
    },

    _animatedFlyPanEnd: function() {
        this._flyTo.panEnd = true;
        this.off('moveend', this._animatedFlyPanEnd, this);
        this._animatedFly();
    },


    flyTo: function(latlng, toZoom) {
        this._flyTo = {latlng: latlng, zoom: toZoom, panEnd: false, zoomEnd: false};
        this._animatedFly();
    }

});
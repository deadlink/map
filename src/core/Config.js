L.Config = L.Class.extend({

    _baseLayers: {
        openstreetmap: function() {
            return L.TileLayer.osm.apply(this,arguments);
        },
        geobase: function() {
            return L.TileLayer.geobase.apply(this,arguments);
        },
        google: function() {
            return L.TileLayer.google.apply(this,arguments);
        },
        mapquest: function() {
            return L.TileLayer.mapQuest.apply(this,arguments);
        },
        yandex: function() {
            return L.TileLayer.yandex.apply(this,arguments);
        },
        nokiahere: function() {
            return L.TileLayer.nokiaHere.apply(this,arguments);
        },
        decarta: function() {
            return L.TileLayer.decarta.apply(this,arguments);
        }
    },

    //Default Params
    center: [53.20959236418347, 50.19953727722168],
    zoom: 8,
    availableLayers: {'openstreetmap':{}, 'geobase':{}, 'google':{}, 'mapquest':{}, 'yandex':{}, 'nokiahere':{appid:'YvxWYaFH39qLe5JOx7zv', token:'Uw8kbrT538rjIdEvWpJziA', lang:'RUS'}, 'decarta':{}},
    activeBaseLayer: 'openstreetmap',
    routerProvider: '/gt/gt-api/geocodings/getRoutePoints',

    getCenter: function() {
        return this.center;
    },
    getZoom: function() {
        return this.zoom;
    },
    getBaseLayers: function() {
        var layers = {};
        for (var key in this.availableLayers) {
            if (key in this._baseLayers) {
                layers[key] = this._baseLayers[key].call(this, this.availableLayers[key]);
            }
        }
        return layers;
    },
    getActiveBaseLayer: function() {
        return this.activeBaseLayer;
    },
    getRouterProvider: function() {
        return this.routerProvider;
    }
});
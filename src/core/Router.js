L.Router = L.Class.extend({

    initialize: function(config) {
        this._provider = config.getRouterProvider();
        console.log(this._provider);
    },

    findRoute: function(from, to, success, index, error) {
        var points = [],
            scope = this;
        jQuery.getJSON(this._provider, {
            fromLat: from.lat ? from.lat : from[0],
            fromLon: from.lng ? from.lng : from[1],
            toLat: to.lat ? to.lat : to[0],
            toLon: to.lng ? to.lng : to[1]
        })
            .done(function(data) {
                if (data.rows && data.rows.length) {
                    for (var i = 0; i < data.rows.length; i++) {
                        var point = scope._getLatLng(data.rows[i]);
                        if (point) {
                            points.push(point);
                        }
                    }
                } else {
                    points = [from, to];
                }
                success(points, index);
            })

            .fail(function(xhr, status, err) {
                if (error) {
                    error([status, err].join(' ,'));
                } else {
                    success([from, to], index)
                }
            });
    },
    _getLatLng: function(point) {
        if (point && point.latitude && point.longitude) {
            return [parseFloat(point.latitude), parseFloat(point.longitude)];
        }
         return null;
    }
});
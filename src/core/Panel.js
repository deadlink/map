L.Panel = L.Class.extend({
    includes: [L.Mixin.Events],
    statics: {},

    _element: null,
    _className: null,
    _position: null,
    _content : null,
    _controlItems: {},

    options: {
        className: 'leaflet-panel',
        style: {},
        isControl: false
    },

    initialize: function(options) {
        L.setOptions(this, options);
    },

    onAdd: function(map) {
        this._map = map;
        this._build();
    },

    addTo: function (map) {
        map.addLayer(this);
        return this;
    },

    _build: function() {
        var map = this._map,
            options = this.options;
        this._element = L.DomUtil.create('div', '', map._container);
        if (options.style)
        for (var key in options.style) {
            this._element.style[key] = options.style[key];
        }
        if (options.isControl) {
            L.DomUtil.addClass(this._element, 'leaflet-bar leaflet-control');
        } else if (options.className) {
            L.DomUtil.addClass(this._element, options.className);
        }
    },

    update: function(content) {
        this._content = content;
        this._element.innerHTML = content;
    },

    addControlItem: function(name, title, className, visible, callback) {
        this._controlItems[name] = {title: title, className: className, callback: callback, visible: visible};
        this.updateControls();
    },

    hideControlItem: function(name) {
        this._controlItems[name].visible = false;
        this.updateControls();
    },

    showControlItem: function(name) {
        this._controlItems[name].visible = true;
        this.updateControls();
    },

    updateControls: function() {
        this._element.innerHTML = '';
        for (var item in this._controlItems) {
            if (this._controlItems[item].visible) {
                var controlItem = L.DomUtil.create('a', this._controlItems[item].className, this._element);
                controlItem.href = "#";
                controlItem.title = this._controlItems[item].title;
                L.DomEvent.addListener(controlItem, 'click', this._controlItems[item].callback, this);
            }
        }
    },

    show: function() {
        this._element.style.visibility = 'visible';
    },

    hide: function() {
        this._element.style.visibility = 'hidden';
    },

    isShown: function() {
        return !this._element.style.visibility === 'visible';
    },

    setPosition: function(point, fix) {
        point = L.point(point);
        var map = this._map;
        this._position = point;
        var width = this._element.clientWidth + this._element.offsetLeft;
        var height = this._element.clientHeight + this._element.offsetTop;
        if (fix) {
            if (point.x + width >= map._container.clientWidth) {
                point.x -= width;
            }
            if (point.y + height >= map._container.clientHeight) {
                point.y -= height;
            }
        }
        L.DomUtil.setPosition(this._element, point)
    },

    getPosition: function() {
        return this._position;
    },

    getContent: function() {
        return this._content;
    }

});

L.panel = function(options) {
    return new L.Panel(options);
}
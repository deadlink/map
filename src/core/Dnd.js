L.Marker.include({
    _onDndInit: function(ev) {
        L.DomEvent.preventDefault(ev);
        L.DomEvent.stopPropagation(ev);
    },
    _onDndStart: function(ev) {
        ev.dataTransfer.setData("text/plain", "dnd");
        ev.dataTransfer.effectAllowed = 'copy';
        return true;
    }
});
L.Marker.addInitHook(function() {
    if (this.options.dnd) {
        this._icon.setAttribute('draggable', 'true');
        L.DomUtil.addClass(this._icon, "dnd");
        L.DomEvent.on(this._icon, 'mousedown', this._onDndInit, this);
        L.DomEvent.on(this._icon, 'dragstart', this._onDndInit, this);
        L.DomEvent.on(this._icon, 'movestart', this._onDndInit, this);
        L.DomEvent.on(this._icon, 'dragstart', this._onDndStart, this);
    }
});

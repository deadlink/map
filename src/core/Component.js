L.Component = L.Class.extend({
    statics: {
        layers: {},
        _events: {
            'click': false,
            'dblclick': false,
            'mousedown': false,
            'mouseup': false,
            'mouseover': false,
            'mouseout': false,
            'mousemove': false,
            'edit': false
        },
        injection: {
            hide: function() {
                if (this._map) {
                    this._map.removeLayer(this);
                }
            },
            show: function() {
                this._map.addLayer(this);
            },
            setData: function(data) {
                this.options.data = data;
            },
            getData: function() {
                return this.options.data? this.options.data: null;
            },
            bindMenu: L.ContextMenu.injection.bindMenu
        },
        initHook: function() {
            // listeners
            for (var option in this.options) {
                if (option in L.Component._events) {
                    this.on(option, this.options[option]);
                }
            }
            // add to group
            if (this.options.group) {
                if (this.options.group in L.Component.layers) {
                    L.Component.layers[this.options.group].addLayer(this);
                }
            }
            // bind contextmenu
            if (this.options.contextMenu && this.options.contextMenu instanceof L.ContextMenu) {
                this.bindMenu(this.options.contextMenu);
            }
            // bind Label
            if (this.options.label) {
                this.bindLabel(this.options.label);
            }
        }
    }
});

L.Map.include({
    addGroup: function(name, group, addToControl, hidden) {
        if (group instanceof L.Class) {
            L.Component.layers[name] = group;
        } else {
            L.Component.layers[name] = L.layerGroup();
        }
        L.Component.layers[name].setName(name);
        if (addToControl) {
            this.addOverlay(L.Component.layers[name], name)
        }
        if (!hidden) {
            L.Component.layers[name].addTo(this);
        }
    },
    removeGroup: function(name) {
        if (name in L.Component.layers) {
            this.removeLayer(L.Component.layers[name]);
            delete L.Component.layers[name];
        }
    },
    getGroup: function(name) {
        if (name in L.Component.layers) {
            return L.Component.layers[name];
        }
        return null;
    },
    bindMenu: L.ContextMenu.injection.bindMenu
});

L.Marker.include(L.Component.injection);
L.Marker.addInitHook(L.Component.initHook);
L.Path.include(L.Component.injection);
L.Path.addInitHook(L.Component.initHook);
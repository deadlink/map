L.Util.getRandomLatLng = function (map) {
    var bounds = map.getBounds(),
        southWest = bounds.getSouthWest(),
        northEast = bounds.getNorthEast(),
        lngSpan = northEast.lng - southWest.lng,
        latSpan = northEast.lat - southWest.lat;
    return [southWest.lat + latSpan * Math.random(),
        southWest.lng + lngSpan * Math.random()];
};

L.Util.boundsToRtreeSearchObject = function(bounds) {
    return {
        x: bounds.getSouthWest().lat,
        y: bounds.getSouthWest().lng,
        w: Math.abs(bounds.getSouthWest().lat - bounds.getNorthEast().lat),
        h: Math.abs(bounds.getSouthWest().lng - bounds.getNorthEast().lng)
    }
};

L.Util.geoRouter = function(a, b, callback) {
    var url = '/viaroute?loc={0},{1}&loc={2},{3}',
        query = url.replace('{0}', a[0]).replace('{1}', a[1]).replace('{2}', b[0]).replace('{3}', b[1]);
    $.getJSON(query, function(data) {
        if (data.route_geometry.length) {
            callback(M.prototype.decodeRouteGeometry(data.route_geometry, 5));
        } else {
            callback(null);
        }
    });
};

L.Util.decodeRouteGeometry = function(encoded, precision) {
    precision = Math.pow(10, -precision);
    var len = encoded.length, index=0, lat=0, lng = 0, array = [];
    while (index < len) {
        var b, shift = 0, result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        //array.push( {lat: lat * precision, lng: lng * precision} );
        array.push( [lat * precision, lng * precision] );
    }
    return array;
};

L.Util.catmullRomSplineInterpolation = function(points, segments) {
    var path = [];
    var calculate = function(t, p1,p2,p3,p4){var t2=t* t,t3=t2*t;
        return 0.5*((2.0*p2)+(p3-p1)*t+(2.0*p1-5.0*p2+4*p3-p4)*t2+(3.0*p2-3.0*p3+p4-p1)*t3);
    };
    var  mu, mudelta, x1, y1, x2, y2, n, h;
    if (points.length < 4 || points.length > 16383) return points;
    mudelta = 1/segments;
    for (n=3; n < points.length; n++) {
        mu = 0;
        x1 = calculate (mu,points[n-3][0],points[n-2][0],points[n-1][0],points[n][0]);
        y1 = calculate (mu,points[n-3][1],points[n-2][1],points[n-1][1],points[n][1]);
        mu = mu + mudelta;
        //path.push([x1, y1]);
        for (h = 0; h < segments; h++) {
            x2 = calculate (mu,points[n-3][0],points[n-2][0],points[n-1][0],points[n][0]);
            y2 = calculate (mu,points[n-3][1],points[n-2][1],points[n-1][1],points[n][1]);
            path.push([x1, y1]);
            mu = mu+mudelta;
            x1 = x2;
            y1 = y2;
        }
        path.push([x2, y2]);
    }
    return path;
};

// Пересечение окружности отрезком p1p2
L.Util.checkCircleIntersection = function(p1, p2, center, radius) {
    var x1 = p1.x-center.x, y1 = p1.y-center.y,
        x2 = p2.x-center.x, y2 = p2.y-center.y,
        dx = x2-x1, dy=y2-y1,
        a = dx*dx + dy*dy,
        b = 2*(x1*dx + y1*dy),
        c = x1*x1 + y1*y1 - radius*radius;
    if (-b < 0) {
        return (c<0);
    }
    if (-b <= (2*a)) {
        return (4*a*c-b*b < 0);
    }
    return (a+b+c < 0);
};

L.Util.createColoringPie = function(colors) {
    var canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        height = 12,
        width = 12,
        last = 90;
    canvas.height = height;
    canvas.width = width;
    ctx.clearRect(0, 0, width, height);
    ctx.strokeStyle = "#ffffff";
    for (var i = 0; i < colors.length; i++) {
        ctx.fillStyle = colors[i];
        ctx.beginPath();
        ctx.moveTo(width / 2, height / 2);
        ctx.arc(width / 2, height / 2, (height + width) / 4 - 1, last,last + (Math.PI*2*(1/colors.length)),false);
        ctx.lineTo(width / 2, height / 2);
        ctx.fill();
        last += Math.PI*2*(1 / colors.length);
    }
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(width / 2, height / 2, (height + width) / 4 - 1, 0, Math.PI*2);
    ctx.stroke();
    ctx.closePath();
    return canvas.toDataURL('image/png');
};


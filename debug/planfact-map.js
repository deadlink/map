Ext.ns('mx.app.widget');

mx.app.widget.PlanFactMap = Ext.extend(mx.comp.NonVisual, {

    MAP_WINDOW_URL:'http://10.11.12.189:80/debug/planfact.html',
    mapWindow: null,
    map:null,
    L: null,
    runId: null,
    resourceId: null,


    // layers
    routeLayer: null,
    tempLayer: null,
    geozonesLayer: null,
    planRouteLayer: null,
    editableLayer: null,
    helperLayer: null,
    tooltip: null,
    geoZoneMenu: null,
    // RTree store
    searchStore: null,
    store: null,
    editedGeozones: {},
    geoZonesList: {},
    geoZonesCount: {},
    centered: false,
    min: null,
    max: null,
    offsetBounds: [2, 2],

    //protected
    initComponent:function () {
        this.addEvents(
            'runLoaded',
            'updateGeoZone'
        );
        mx.app.widget.PlanFactMap.superclass.initComponent.apply(this, arguments);
    },

    //private
    isActive:function () {
        try {
            if(this.mapWindow && this.mapWindow.isOnline()) {
                return true;
            }
        } catch(ev) {
            return false;
        }
        return false;
    },


    getLatLng: function(point) {
        return [parseFloat(point.latitude), parseFloat(point.longitude)];
    },

    getDistinct: function(points) {
        if (!points || !points.length || !points[0].b) return points;
        var stamp = points[0].b.date,
            a = [], b = [];
        for (var i = 0; i < points.length; i++) {
            if (~~((points[i].b.date - stamp) / 600000) == 0) {
                a.push(points[i]);
            } else {
                b.push(points[i])
            }
        };
        var comparefn = function(p1, p2){
            return p1.date < p2.date;
        };
        a.sort(comparefn);
        b.sort(comparefn);
        if (a.length && b.length) {
            return [a[0], b[0]];
        } else {
            return [a[0]];
        }
    },

    init: function(title, callback) {
        var url = this.MAP_WINDOW_URL + "&d=" + new Date().getTime();
        var scope = this;
        this.mapWindow = window.open(url, '_blank', "location=no,menubar=no,resizable=yes,width=1024,height=650");
        this.mapWindow.callback =  function(ev) {
            scope.map = scope.mapWindow.init(title, {center: [50, 50]});
            scope.L = scope.mapWindow.L;
            scope.clear(true);
            callback();
            scope.mapWindow.focus();
        };
    },

    clear: function(afterInit) {
        var map = this.map, L = this.L, scope = this;
        if (afterInit) {
            this.routeLayer = L.layerGroup();
            this.planRouteLayer = L.featureGroup();
            this.tempLayer = L.layerGroup();
            this.geozonesLayer = L.layerGroup();
            this.editableLayer = L.layerGroup();
            this.helperLayer = L.layerGroup();
            this.searchStore = map.getStore('searchStore');
            var mapMenu = L.contextMenu({
                // TODO need localization
                items: {
                    'Показать геозоны': function(target) {
                        Ext.Ajax.request({
                            url: mx.PATH + MXAT.API("geozones/listByIds/"),
                            method: "POST",
                            params: {
                                ids: Object.keys(scope.geoZonesList)
                            },
                            success: function(r) {
                                var response = Ext.util.JSON.decode(r.responseText);
                                if (response.rows && response.rows.length) {
                                    for (var i = 0; i < response.rows.length; i++) {
                                        scope.geoZonesList[response.rows[i].id].geometry = response.rows[i];
                                    }
                                    scope.drawGeozonesGeometry();
                                }
                            }
                        });
                    },
                    'Редактироать геозоны': function() {
                        scope.geozonesLayer.clear();
                        scope.planRouteLayer.clear();
                        scope.editableLayer.eachLayer(function(geozone) {
                            geozone.editing.options.moveIcon = scope.geoZonesList[geozone.getData()].icon;
                            geozone.editing.enable();
                        });
                    },
                    'Отредактированные геозоны': function() {
                        console.log(scope.editedGeozones);
                    },
                    'Сохранить позицию геозон': function() {
                        var records = [];
                        for (var id in scope.editedGeozones) {
                            var record = scope.geoZonesList[id].geometry;
                            var updated = scope.editedGeozones[id];
                            scope.geoZonesList[id].latitude = updated.center.lat;
                            scope.geoZonesList[id].longitude = updated.center.lng;
                            record.centerLat = updated.center.lat;
                            record.centerLon = updated.center.lng;
                            record.radiusLat = updated.radius.lat;
                            record.radiusLon = updated.radius.lng;
                            record.toRemove = false;
                            record.mapId = 1;
                            records.push(record);
                        }
                        Ext.Ajax.request({
                            url: mx.PATH + MXAT.API("geozones/massupdate/"),
                            method: "POST",
                            jsonData: {records: records},
                            success: function(response) {
                                scope.fireEvent('updateGeoZone');
                                scope.centered = true;
                                scope.geozonesLayer.clear();
                                scope.editableLayer.clear();
                                scope.drawGeozones();
                                scope.drawPlanRoute();
                            },
                            failure: function() {
                            }
                        });
                    },
                    'Показать лэндмарки': function(target) {
                        var landmarkStore = mx.data.Landmark.newStore({
                            url:MXAT.API("landmarks/all")
                        });
                        landmarkStore.load({
                            callback:function(r){
                                for (var i = 0; i < r.length; i++) {
                                    L.circleMarker(scope.getLatLng(r[i].get('address').data), {
                                        group: 'geozones',
                                        weight: 0,
                                        opacity: 0.9,
                                        fillOpacity: 0.5,
                                        color: 'green',
                                        radius: 5,
                                        data: r[i],
                                        mouseover: function(ev) {
                                            scope.tooltip.hide();
                                            scope.tooltip.update(ev.target.getData().get('name'));
                                            scope.tooltip.setPosition(map.latLngToContainerPoint(ev.target.getLatLng()));
                                            scope.tooltip.show();
                                        },
                                        mouseout: function() {
                                            scope.tooltip.hide();
                                        }
                                    });
                                }

                            },
                            scope:this
                        });
                    }
                }
            }).addTo(map);
            map.bindMenu(mapMenu);
            map.addGroup('route', this.routeLayer);
            map.addGroup('planRoute', this.planRouteLayer);
            map.addGroup('temp', this.tempLayer);
            map.addGroup('geozones', this.geozonesLayer);
            map.addGroup('editable', this.editableLayer);
            map.addGroup('helper', this.helperLayer);
            this.tooltip = L.panel({style:{top:'10px', left: '15px'}}).addTo(map);
        }

        this.geoZonesList = {};
        this.editedGeozones = {};
        this.centered = false;
        this.routeLayer.clear();
        this.planRouteLayer.clear();
        this.tempLayer.clear();
        this.geozonesLayer.clear();
        this.editableLayer.clear();
        this.helperLayer.clear();
        this.searchStore.clear();
        this.offsetBounds = [2, 2];
    },

    setGeoZones: function(geoZonesList) {
        console.log(geoZonesList);
        this.geoZonesCount = geoZonesList.length;
        for (var i = 0; i < geoZonesList.length; i++) {
            var zone = geoZonesList[i].data;
            this.geoZonesList[zone.id] = zone;
        }
    },

    drawGeozones: function() {
        var map = this.map, L = this.L, scope = this,
            geoZonesList = this.geoZonesList, pointTypes = [], type = "";
        for (var id in geoZonesList) {
            pointTypes = geoZonesList[id].pointTypes;
            type = "";
            if (pointTypes && pointTypes.length) {
                for(var i=0; i<pointTypes.length; i++) {
                    if (pointTypes[i].get('name') == 'DROP') {
                        type = geoZonesList[id].plannedNumber;
                    } else {
                        type += ' ' + mx.translate('pointTypeShort.' + pointTypes[i].get('name'));
                    }

                }
            };
            geoZonesList[id].icon = L.divIcon({
                iconSize: new L.Point(27, 27),
                className: 'geozoneIcon',
                html: type,
                riseOnHover: true
            });

            L.marker(this.getLatLng(geoZonesList[id]), {
                group: 'geozones',
                riseOnHover: true,
                data: id,
                icon: geoZonesList[id].icon,
                mouseover: function(ev) {
                    scope.tooltip.hide();
                    scope.tooltip.update(L.Util.template("{name}<br>{address}", {
                        name: geoZonesList[ev.target.getData()].locationName,
                        address: geoZonesList[ev.target.getData()].locationAddress

                    }));
                    scope.tooltip.setPosition(map.latLngToContainerPoint(ev.target.getLatLng()));
                    scope.tooltip.show();
                },
                mouseout: function(ev) {
                    scope.tooltip.hide();
                }
            });
        }
    },


    drawGeozonesGeometry: function() {
        var map = this.map, L = this.L, scope = this,
            geoZonesList = this.geoZonesList, geometry = null;
        for (var id in geoZonesList) {
            geometry = geoZonesList[id].geometry;
            switch (geometry.type) {
                case 'sphere':
                    var center = L.latLng([geometry.centerLat, geometry.centerLon]),
                        radius = L.latLng([geometry.radiusLat, geometry.radiusLon]);
                    L.circle(center, center.distanceTo(radius), {
                        group: 'editable',
                        color: 'red',
                        weight: 3,
                        opacity: 0,
                        edit: function(ev) {
                            var id = ev.target.getData();
                            scope.editedGeozones[id] = {
                                center: ev.target.getLatLng(),
                                radius: L.latLng([
                                    ev.target.getBounds().getSouthWest().lat,
                                    ev.target.getLatLng().lng]
                                )
                            };
                        },
                        data: id
                    });
                    break;
            };
        }
    },

    drawPlanRoute: function() {
        var map = this.map, L = this.L, scope = this,
            geoZonesList = this.geoZonesList,
            planTrackPoints = [],
            pointsForRoute = this.geoZonesCount - 1,
            lastPoint = null, point = null, planTrackLines = [], temp = null;
        for (var id in geoZonesList) {
            point = geoZonesList[id];
            if (lastPoint) {
                map.getRouter().findRoute(
                    this.getLatLng(lastPoint),
                    this.getLatLng(point),
                    function(points) {
                        pointsForRoute--;
                        planTrackPoints.push(points);
                        L.polyline(points, {
                            group: 'planRoute',
                            color: 'blue',
                            weight: 5,
                            opacity: 0.7
//                            data: {
//                                a: scope.getLatLng(lastPoint),
//                                b: scope.getLatLng(point)
//                            },
//                            mouseover: function(ev) {
//                                temp = L.marker(ev.latlng, {
//                                    icon: L.divIcon(),
//                                    group: 'temp',
//                                    radius: 10,
//                                    dashArray: [1, 2],
//                                    mouseout: function(ev) {
//                                        if (!temp.drag) {
//                                            temp.hide();
//                                        }
//                                    },
//                                    draggable: true,
//                                    data: ev.target
//                                });
//                                temp.dragging.enable();
//                                temp.on('dragstart', function() {
//                                    temp.drag = true;
//                                });
//                                temp.on('dragend', function(ev) {
//                                    temp.drag = false;
//                                    map.getRouter().findRoute(temp.getData().getData().a, temp.getLatLng(), function(points) {
//                                        L.polyline(points, {
//                                            group: 'planRoute',
//                                            color: 'red',
//                                            weight: 5,
//                                            opacity: 0.7
//                                        });
//                                    });
//                                    map.getRouter().findRoute(temp.getLatLng(), temp.getData().getData().b, function(points) {
//                                        L.polyline(points, {
//                                            group: 'planRoute',
//                                            color: 'red',
//                                            weight: 5,
//                                            opacity: 0.7
//                                        });
//                                    });
//                                    temp.hide();
//                                });
//                            }
                        });
                        if (!pointsForRoute) {
                            if (!scope.centered) {
                                map.fitBounds(scope.planRouteLayer.getBounds());
                            }
                        }
                    }
                );
            }
            lastPoint = point;
        }
    },

    drawFactRoute: function(params) {
        var scope = this, map = this.map, L = this.L;
        this.resourceId = params.resourceId;
        this.store = mx.data.GeoPoint.newStore({url:MXAT.API("planfact/pointsForRun/"+params.resourceId)});
        console.log(params);
        this.store.load({
            params: params,
            callback: function(data) {
                scope.prepareRoute(data);
            }
        });
    },

    prepareRoute: function(data) {
        if (data.length == 0) return;
        var scope = this, map = this.map, L = this.L, points = [];
        for (var i = 0; i < data.length - 1; i++) {
            // Берем текущую и следующую точки
            var current = data[i].data,
                next = data[i+1].data;

            // Помещаем в store область, в которую входит отрезок от текущей точки до следующей
            this.searchStore.add(new L.LatLngBounds(this.getLatLng(current),this.getLatLng(next)), {
                a: current,
                b: next
            });

            // Формируем массив точек, по которым будем рисовать трек
            points.push(this.getLatLng(current));
        }

        // Помещаем в список последнюю точку, тк мы ее пропустили в цикле
        if (next) {
            points.push(this.getLatLng(next));
        }

        // Инициализируем слайдер
        // Документация по слайдеру: http://ghusse.github.io/jQRangeSlider/documentation.html
        var roundHour = function(val, high) {
            return high ? Math.round(val / sinh) * sinh : Math.floor(val / sinh) * sinh;
        };
        var min = data[0].get('date').getTime();
        var max = data[data.length - 1].get('date').getTime();
        scope.min = min;
        scope.max = max;
        scope.initSlider(min, max, data);
        scope.drawHelperLayer(points);
        scope.updateRoute(points);
    },

    initSlider: function(min, max, data) {
        var scope = this,
            roundHour = function(val, high) {
                return high ? Math.round(val / sinh) * sinh : Math.floor(val / sinh) * sinh;
            },
            offset = (new Date()).getTimezoneOffset()*60*1000,
            sinh = 3600000,
            bounds = [roundHour(max + sinh*scope.offsetBounds[1], true), roundHour(min - sinh*scope.offsetBounds[0], false)],
            step = (bounds[0] - bounds[1]) / 100;
        this.mapWindow.initSlider({
                arrows: false,
                bounds:{
                    min: 0,
                    max: 100
                },
                defaultValues: {
                    min: (min - bounds[1]) / step,
                    max: (bounds[0] - min) / step
                },
                valueLabels:"change",
                delayOut: 4000,

                formatter:function(val){
                    return (new Date(bounds[1]+val*step)).format('H:i');
                },
                scales: [
                    {
                        first: function(val){ return val; },
                        next: function(val){
                            return val + 10;
                        },
                        stop: function(val){ return false; },
                        label: function(val){
                            return (new Date(bounds[1]+val*step)).format('H:i');
                        }
                    }
                ]
            },
            // onChange
            function(ev, cdata) {
                var points = [];
                scope.min = cdata.values.min*step + bounds[1];
                scope.max = cdata.values.max*step + bounds[1];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].get('date').getTime() >= (cdata.values.min*step + bounds[1]) &&
                        data[i].get('date').getTime() <= cdata.values.max*step + bounds[1]) {
                        points.push(scope.getLatLng(data[i].data));
                    }
                }
                scope.updateRoute(points);
            },
            // onChanged
            function(ev, cdata) {
                var range = null;
                var feature = false;
                if (scope.min < min) {
                    range = [scope.min, min];
                } else if (scope.max > max){
                    feature = true
                    range = [max, scope.max];
                }
                if (range) {
                    var store = mx.data.GeoPoint.newStore({url:MXAT.API("planfact/pointsForRange/"+scope.resourceId)});
                    store.load({
                        params: {
                            rangeStart: parseInt(range[0]) - offset,
                            rangeEnd: parseInt(range[1]) - offset
                        },
                        callback: function(_data) {
                            if (_data.length) {
                                console.log(_data, feature);
                                if (feature) {
                                    max = scope.max;
                                    data = data.concat(_data);
                                } else {
                                    min = scope.min;
                                    data = _data.concat(data);
                                }
                            }
                        }
                    });
                }
            },
            function(offsetBounds) {
                scope.offsetBounds[0] += offsetBounds[0];
                scope.offsetBounds[1] += offsetBounds[1];
                scope.initSlider(min, max, data);
            }
        );
    },

    drawHelperLayer: function(points) {
        var scope = this, map = this.map, L = this.L;
        var segments = [];
        var lastSegments = [];
        var clear= function() {
            if (segments.length) {
                for (var i = 0; i < segments.length; i++) {
                    segments[i].hide();
                }
                segments = [];
            }
        }
        scope.helperLayer.clear();
        var helper = L.polyline(points, {
            group: 'helper',
            weight: 12,
            color: 'transparent',
            opacity: 0,
            mousemove: function(ev) {
                var point = ev.containerPoint,
                    delta = 10,
                    sigma = 5,
                    sw = map.containerPointToLatLng([point.x - sigma, point.y + sigma]),
                    ne = map.containerPointToLatLng([point.x + sigma, point.y - sigma]),
                    searchBounds = new L.LatLngBounds(sw, ne),
                    geoPoints =[];
                scope.searchStore.search(searchBounds, false, geoPoints);
                geoPoints = scope.getDistinct(geoPoints);
                if (geoPoints && geoPoints.length) {
                    var points = [];
                    for (var i = 0; i < geoPoints.length; i++) {
                        if (
                            geoPoints[i].a.date.getTime() >= scope.min && geoPoints[i].a.date.getTime() <= scope.max &&
                                geoPoints[i].b.date.getTime() >= scope.min && geoPoints[i].b.date.getTime() <= scope.max &&
                                L.Util.checkCircleIntersection(
                                    map.latLngToContainerPoint(scope.getLatLng(geoPoints[i].a)),
                                    map.latLngToContainerPoint(scope.getLatLng(geoPoints[i].b)),
                                    point,
                                    delta
                                )) {
                            points.push(geoPoints[i]);
                        }
                    }
                    if (points.length) {
                        if (lastSegments != points) {
                            clear();
                        }
                        var text = [];
                        for (var i = 0; i < points.length; i++) {
                            var geoPoint = points[i].a;
                            var label = L.Util.template("Скорость: {speed} km/h <br> " +
                                "<img style=\"float: right; -webkit-transform:rotate({azimut}deg);\" src=\"http://10.11.12.189:80/azimut.png\">" +
                                'GPS: {lat}, {lng}, {azimut}<br>' +
                                'Date: {date}', {
                                speed: ~~(geoPoint.speed*100)/100,
                                azimut: geoPoint.heading,
                                lat: parseFloat(geoPoint.latitude).toFixed(6),
                                lng: parseFloat(geoPoint.longitude).toFixed(6),
                                date: (new Date(geoPoint.date - (new Date()).getTimezoneOffset())).format('H:i:s d.m.Y')
                            });
                            text.push(label);
                            var segment = L.polyline([scope.getLatLng(points[i].a),scope.getLatLng(points[i].b)], {
                                group: 'temp',
                                weight: 7,
                                color: 'red'
                            }).bringToBack();
                            segments.push(segment);
                        }
                        if (lastSegments != points) {
                            lastSegments = points;
                            scope.tooltip.update(text.join('<hr>'));
                        }
                        scope.tooltip.setPosition(point);
                        scope.tooltip.show();

                    }
                }
            },
            mouseout: function() {
                clear();
                scope.tooltip.hide();
            }
        }).bringToFront();
        scope.centered = true;
        map.fitBounds(helper.getBounds())
    },

    updateRoute: function(points) {
        var L = this.L;
        this.routeLayer.clear();
        L.polyline(points, {
            group: 'route',
            weight: 5,
            color: 'red'
        }).bringToBack();
    },


    centerOn: function(point) {
        this.centered = true;
        this.map.flyTo(this.getLatLng(point), 15);
    }
});

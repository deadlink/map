/**
 * User: deadlink
 * Date: 04.04.13
 * Time: 9:32
 */
function addScript(file) {
    document.write("<script type=\"text/javascript\" src=\"/src/"+file+"\"></script>");
}

function addStyle(file) {
    document.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"/src/"+file+"\">");
}

for (var group in deps) {
    if (deps[group].src) {
        deps[group].src.forEach(addScript);
    }
    if (deps[group].styles) {
        deps[group].styles.forEach(addStyle);
    }
}
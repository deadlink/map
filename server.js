var http = require('http');
var fs = require('fs');
var port = 3003;
var OSRM_HOST = '192.0.2.61';
var OSRM_PORT = 5000;
var mimeTypes = {
    "html": "text/html",
    "js": "text/javascript",
    "css": "text/css",
    "json": "application/json",
    "png": "image/png",
    "jpg": "image/jpg",
    "ico": "image/ico",
    "woff": "application/x-font-woff",
    "svg": "image/svg",
    "ttf": "image/ttf"
};
var binaryTypes = [mimeTypes.png, mimeTypes.jpg, mimeTypes.woff,mimeTypes.svg, mimeTypes.ttf];
function sendFile(response, path, encoding, type) {
    fs.readFile(path, encoding, function (err, data) {
        if (err) {
            response.writeHead(404);
            response.write('Not found');
            response.end();
            return;
        }
        response.writeHead(200, {"Content-type" : type});
        response.write(data, encoding);
        response.end();
    });
}

http.createServer(function(request, response) {

//    response.writeHead(204);
//    response.end();
//    return;

    if (/viaroute/.test(request.url)) {
        http.get ({
            host: OSRM_HOST,
            port: OSRM_PORT,
            path: request.url
        }, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                response.write(chunk)
            });
            res.on('end', function(){
                response.end()
            });
            response.writeHead(res.statusCode, {'Content-type': mimeTypes.json});
        }).on('error', function(e) {
            console.log("error: " + e.message);
        });
        return;
    }
//
//    if (/wms/.test(request.url)) {
//        http.get ({
//            host: 'localhost',
//            port: '3128',
//            path: 'http://geoportal.samregion.ru'+request.url,
//            referrer: 'http://geoportal.samregion.ru/ecko/'
//        }, function (res) {
//            res.setEncoding('binary');
//            res.on('data', function (chunk) {
//                response.write(chunk)
//            });
//            res.on('end', function(){
//                response.end()
//            });
//            response.writeHead(res.statusCode, {'Content-type': mimeTypes.png});
//        }).on('error', function(e) {
//                console.log("error: " + e.message);
//            });
//        return;
//    };
    var path = request.url.match(/[^\?&]+/)[0];
    var temp = path.match(/\.([^\.]+)$/);
    var type = "text/plain";
    var encoding = 'utf-8';
    if (temp && temp.length) {
        if (temp[1] in mimeTypes) {
            type = mimeTypes[temp[1]];
        }
    }
    if (binaryTypes.indexOf(type) != -1) {
        encoding = 'binary';
    }

    console.log(process.pid, 'Request:', path, 'mime:', type, 'encoding:', encoding);
    sendFile(response, '.'+path, encoding, type);
}).listen(port);

var deps = {

    Leaflet: {
        src: [
            'lib/leaflet/leaflet-src.js',
            'lib/leaflet/plugins/label/leaflet.label.js',
            'lib/leaflet/plugins/markercluster/leaflet.markercluster-src.js',
            'lib/leaflet/plugins/draw/leaflet.draw-src.js'
        ],
        styles: [
            'lib/leaflet/leaflet.css',
            'lib/leaflet/plugins/label/leaflet.label.css',
            'lib/leaflet/plugins/markercluster/MarkerCluster.Default.css',
            'lib/leaflet/plugins/draw/leaflet.draw.css'
        ],
        desc: 'Leaflet and plugins'
    },

    Shim: {
        src: [
            'lib/es5-shim.min.js'
        ],
        desc: 'EcmaScript 5 methods emulation'
    },

    RTree: {
        src: [
            'lib/rtree.js'
        ],
        desc: 'RTree'
    },

    Proj4j: {
        src: [
            'lib/proj4js-compressed.js',
            'lib/proj4leaflet.js'
        ],
        desc: 'Proj4 projection libruary'
    },

    Core: {
        src: [
            'core/Layer.js',
            'core/Utils.js',
            'core/Selection.js',
            'core/Store.js',
            'core/Panel.js',
            'core/ContextMenu.js',
            'core/Component.js',
            'core/Dnd.js',
            'core/Config.js',
            'core/Marker.js',
            'core/Router.js',
            'core/Map.js'
        ],
        desc: 'Core',
        deps: ['Shim', 'RTree'],
        styles: [
            'css/style.css',
            'css/Panel.css',
            'css/ContextMenu.css',
            'css/font-awesome.min.css'
        ]
    },

    Maxoptra: {
        src: [
            'core/MaxoptraConfig.js'
        ],
        deps: 'Core',
        desc: 'Maxoptra'
    },

    TileSources: {
        src: [
            'tile/Google.js',
            'tile/OSM.js',
            'tile/MapQuest.js',
            'tile/NokiaHere.js',
            'tile/Yandex.js',
            'tile/Decarta.js',
            'tile/Geobase.js'
        ],
        deps: ['Core'],
        desc: ['Tile providers']
    }
};

if (typeof exports !== 'undefined') {
	exports.deps = deps;
}
